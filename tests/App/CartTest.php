<?php

use App\Cart;
use App\Product\Product;
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase
{
    public function getTotalDataProvider()
    {
        return [
            'B01,G01' => [
                [
                    new Product('Blue Widget', 'B01', 7.95),
                    new Product('Green Widget', 'G01', 24.95),
                ],
                37.85
            ],
            'R01,R01' => [
                [
                    new Product('Red Widget', 'R01', 32.95),
                    new Product('Red Widget', 'R01', 32.95),
                ],
                54.37
            ],
            'R01,G01' => [
                [
                    new Product('Red Widget', 'R01', 32.95),
                    new Product('Green Widget', 'G01', 24.95),
                ],
                60.85
            ],
            'B01,B01,R01,R01,R01' => [
                [
                    new Product('Blue Widget', 'B01', 7.95),
                    new Product('Blue Widget', 'B01', 7.95),
                    new Product('Red Widget', 'R01', 32.95),
                    new Product('Red Widget', 'R01', 32.95),
                    new Product('Red Widget', 'R01', 32.95),
                ],
                98.27
            ],
        ];
    }

    /**
     * @dataProvider getTotalDataProvider
     * @param Product[] $products
     * @param float $total
     */
    public function testGetTotal(array $products, float $total)
    {
        $cart = new Cart($products);
        self::assertEquals($total, $cart->getTotal());
    }
}
