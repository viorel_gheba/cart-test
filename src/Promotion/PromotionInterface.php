<?php

namespace App\Promotion;

use App\Product\Product;

interface PromotionInterface
{
    /**
     * Check if this promotion can be applied
     * @param Product[] $products
     * @return bool
     */
    public function canApply(array $products): bool;

    /**
     * Apply promotion
     * @param Product[] $products
     */
    public function applyPromotion(array $products): void;
}
