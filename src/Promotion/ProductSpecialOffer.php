<?php

namespace App\Promotion;

use App\Product\Product;

class ProductSpecialOffer
{
    private string $productCode;
    private float $discount;

    /**
     * @param string $productCode
     * @param float $discount
     */
    public function __construct(string $productCode, float $discount)
    {
        $this->productCode = $productCode;
        $this->discount = $discount;
    }

    /**
     * @param Product[] $products
     * return array
     */
    private function getFilteredProducts(array $products)
    {
        $productCode = $this->productCode;
        return array_values(
            array_filter(
                $products,
                function ($productDto) use ($productCode) {
                    return $productDto->getCode() === $productCode;
                }
            )
        );
    }

    /**
     * @param Product[] $products
     * @return bool
     */
    public function canApply(array $products): bool
    {
        return count($this->getFilteredProducts($products)) >= 2;
    }

    /**
     * @param Product[] $products
     */
    public function applyPromotion(array $products): void
    {
        $products = $this->getFilteredProducts($products);

        for ($i = 1, $c = count($products); $i < $c; $i+=2) {
            $product = $products[$i];
            $product->setDiscount($this->discount);
        }
    }
}
