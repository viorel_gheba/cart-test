<?php

namespace App\ShippingTaxRule;

use App\Cart;

interface ShippingTaxRuleInterface
{
    /**
     * Returns the calculated shipping tax
     * @param Cart $cart
     * @return float
     */
    public function getShippingTax(Cart $cart): ?float;
}
