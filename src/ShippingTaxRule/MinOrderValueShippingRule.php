<?php

namespace App\ShippingTaxRule;

use App\Cart;

class MinOrderValueShippingRule implements ShippingTaxRuleInterface
{
    private float $minOrderValue;
    private float $shippingTax;

    /**
     * @param float $minOrderValue
     * @param float $shippingTax
     */
    public function __construct(float $minOrderValue, float $shippingTax)
    {
        $this->minOrderValue = $minOrderValue;
        $this->shippingTax = $shippingTax;
    }

    public function getShippingTax(Cart $cart): ?float
    {
        $orderValue = $cart->getTotalWithoutShipping();
        if (bccomp($orderValue, $this->minOrderValue, 2) === -1) {
            return $this->shippingTax;
        }
        return null;
    }
}
