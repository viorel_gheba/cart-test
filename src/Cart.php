<?php

namespace App;

use App\Product\ProductInterface;
use App\Promotion\ProductSpecialOffer;
use App\Promotion\PromotionInterface;
use App\ShippingTaxRule\MinOrderValueShippingRule;
use App\ShippingTaxRule\ShippingTaxRuleInterface;

class Cart
{
    /** @var ProductInterface[] */
    private array $products = [];

    /** @var ShippingTaxRuleInterface[] */
    private array $shippingRules;

    /** @var PromotionInterface[] */
    private array $promotions = [];

    /**
     * @param ProductInterface[] $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;

        $this->shippingRules = [
            new MinOrderValueShippingRule(50, 4.95),
            new MinOrderValueShippingRule(90, 2.95),
        ];

        $this->promotions = [
            new ProductSpecialOffer('R01', -0.5),
        ];
    }

    private function applyPromotions(): void
    {
        foreach ($this->promotions as $promotion) {
            if (!$promotion->canApply($this->products)) {
                continue;
            }

            $promotion->applyPromotion($this->products);
        }
    }

    public function getTotalWithoutShipping(): float
    {
        $total = 0;

        foreach ($this->products as $product) {
            $total = bcadd($total, $product->getPrice(), 2);
        }

        return $total;
    }

    public function calculateShippingTax(): float
    {
        foreach ($this->shippingRules as $rule) {
            $shippingTax = $rule->getShippingTax($this);
            if ($shippingTax !== null) {
                return $shippingTax;
            }
        }

        return 0;
    }

    public function getTotal(): float
    {
        $this->applyPromotions();
        return bcadd($this->getTotalWithoutShipping(), $this->calculateShippingTax(), 2);
    }
}
