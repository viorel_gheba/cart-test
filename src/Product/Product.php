<?php

namespace App\Product;

class Product implements ProductInterface
{
    private string $name;
    private string $code;
    private float $price;
    private float $discount = 0;

    /**
     * @param string $name
     * @param string $code
     * @param float $price
     */
    public function __construct(string $name, string $code, float $price)
    {
        $this->name = $name;
        $this->code = $code;
        $this->price = $price;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getPrice(): float
    {
        return bcmul($this->price, bcadd(1, $this->discount, 2), 2);
    }

    /**
     * @param float $discount
     */
    public function setDiscount(float $discount): void
    {
        $this->discount = $discount;
    }
}
