DOCKER=$(shell which docker)
PWD=$(shell pwd)

.PHONY: build

build:
	$(DOCKER) build -t cart-test:latest -f Dockerfile .
	$(DOCKER) run --rm -v $(PWD):/opt/project cart-test:latest composer install

run:
	$(DOCKER) run --rm -v $(PWD):/opt/project cart-test:latest php vendor/bin/phpunit -c phpunit.xml
